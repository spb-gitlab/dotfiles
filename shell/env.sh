export PATH=$HOME/.local/bin:$PATH

if [ -f ~/.env_local ]
then
    source ~/.env_local
fi
